package plugin.bonus;

import java.awt.image.BufferedImage;

import plugin.IPlugin;
import plugin.Utils;

public class SolarizeFilter implements IPlugin
{
  private String name_;
  private int[] bias_;

  public SolarizeFilter()
  {
    name_ = "Solarize";
    bias_ = new int[3];

    bias_[0] = 128;
    bias_[1] = 128;
    bias_[2] = 128;
  }

  private int[] invertColors(int[] orig)
  {
    for (int i = 1; i < 4; i++)
    {
      if (orig[i] < bias_[i - 1])
        orig[i] = 255 - orig[i];
    }

    return orig;
  }

  @Override
  public BufferedImage perform(BufferedImage img)
  {
    BufferedImage result = Utils.copyImage(img);
    int[] res = new int[4];

    for (int i = 0; i < img.getWidth(); i++)
    {
      for (int j = 0; j < img.getHeight(); j++)
      {
        for (int c = 0; c < 4; c++)
        {
          res[c] = Utils.extractComp(c, img.getRGB(i, j));
        }

        res = invertColors(res);
        result.setRGB(i, j, Utils.mergeComp(res));
      }
    }

    return result;
  }

  @Override
  public String getName()
  {
    return name_;
  }
}
