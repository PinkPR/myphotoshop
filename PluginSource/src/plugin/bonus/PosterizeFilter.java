package plugin.bonus;

import java.awt.image.BufferedImage;

import plugin.IPlugin;
import plugin.Utils;

public class PosterizeFilter implements IPlugin
{
  private String name_;
  private int[] divs_;

  public PosterizeFilter()
  {
    name_ = "Posterize";
    divs_ = new int[3];

    divs_[0] = 8;
    divs_[1] = 8;
    divs_[2] = 8;
  }

  private int[] posterizeColors(int[] orig)
  {
    for (int i = 1; i < 4; i++)
    {
      while (orig[i] % (256 / divs_[i - 1]) != 0)
        orig[i]++;
    }

    return orig;
  }

  @Override
  public BufferedImage perform(BufferedImage img)
  {
    BufferedImage result = Utils.copyImage(img);
    int[] res = new int[4];

    for (int i = 0; i < img.getWidth(); i++)
    {
      for (int j = 0; j < img.getHeight(); j++)
      {
        for (int c = 0; c < 4; c++)
        {
          res[c] = Utils.extractComp(c, img.getRGB(i, j));
        }

        res = posterizeColors(res);
        result.setRGB(i, j, Utils.mergeComp(res));
      }
    }

    return result;
  }

  @Override
  public String getName()
  {
    return name_;
  }
}
