package plugin.basic;

import java.awt.image.BufferedImage;

import plugin.IPlugin;
import plugin.Utils;

public class InvertFilter implements IPlugin
{
  private String name_;

  public InvertFilter()
  {
    name_ = "Invert";
  }

  private int[] invertColors(int[] orig)
  {
    for (int i = 1; i < 4; i++)
    {
      orig[i] = 255 - orig[i];
    }

    return orig;
  }

  @Override
  public BufferedImage perform(BufferedImage img)
  {
    BufferedImage result = Utils.copyImage(img);
    int[] res = new int[4];

    for (int i = 0; i < img.getWidth(); i++)
    {
      for (int j = 0; j < img.getHeight(); j++)
      {
        for (int c = 0; c < 4; c++)
        {
          res[c] = Utils.extractComp(c, img.getRGB(i, j));
        }

        res = invertColors(res);
        result.setRGB(i, j, Utils.mergeComp(res));
      }
    }

    return result;
  }

  @Override
  public String getName()
  {
    return name_;
  }
}
