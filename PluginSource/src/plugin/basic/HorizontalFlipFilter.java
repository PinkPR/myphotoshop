package plugin.basic;

import java.awt.image.BufferedImage;

import plugin.IPlugin;
import plugin.Utils;

public class HorizontalFlipFilter implements IPlugin
{
  private String name_;

  public HorizontalFlipFilter()
  {
    name_ = "HorizontalFlip";
  }

  @Override
  public BufferedImage perform(BufferedImage img)
  {
    BufferedImage result = Utils.copyImage(img);

    for (int i = 0; i < img.getWidth(); i++)
    {
      for (int j = 0; j < img.getHeight(); j++)
      {
        result.setRGB(img.getWidth() - 1 - i, j, img.getRGB(i, j));
      }
    }

    return result;
  }

  @Override
  public String getName()
  {
    return name_;
  }
}
