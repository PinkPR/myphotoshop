package plugin.basic;

import java.awt.image.BufferedImage;

import plugin.IPlugin;
import plugin.Utils;

public class GrayscaleFilter implements IPlugin
{
  private String name_;
  private float[] divs_;

  public GrayscaleFilter()
  {
    name_ = "Grayscale";
    divs_ = new float[3];

    divs_[0] = 0.2f;
    divs_[1] = 0.7f;
    divs_[2] = 0.1f;
  }

  private int[] grayscaleColors(int[] orig)
  {
    int res = 0;

    for (int i = 1; i < 4; i++)
    {
      orig[i] = (int) (orig[i] * divs_[i - 1]);
      res += orig[i];
    }

    for (int i = 1; i < 4; i++)
    {
      orig[i] = res;
    }

    return orig;
  }

  @Override
  public BufferedImage perform(BufferedImage img)
  {
    BufferedImage result = Utils.copyImage(img);
    int[] res = new int[4];

    for (int i = 0; i < img.getWidth(); i++)
    {
      for (int j = 0; j < img.getHeight(); j++)
      {
        for (int c = 0; c < 4; c++)
        {
          res[c] = Utils.extractComp(c, img.getRGB(i, j));
        }

        res = grayscaleColors(res);
        result.setRGB(i, j, Utils.mergeComp(res));
      }
    }

    return result;
  }

  @Override
  public String getName()
  {
    return name_;
  }
}
