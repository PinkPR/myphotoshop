package plugin.basic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JSlider;
import javax.swing.JButton;

import plugin.IPlugin;
import plugin.Utils;
import plugin.basic.GrayscaleFilter;

public class BinarizeFilter implements IPlugin
{
  /*public class Dialog implements Runnable
  {
    private int valid_ = 0;

    public class OkListener implements ActionListener
    {
      public void actionPerformed(ActionEvent e)
      {
        JButton butt = (JButton) e.getSource();
        valid_ = 1;
      }
    }

    public void run()
    {
      JFrame chooser = new JFrame("Filter Parameters");
      chooser.setSize(200, 100);
      chooser.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
      JSlider slider = new JSlider(JSlider.HORIZONTAL, 1, 200, 15);
      JButton ok = new JButton("OK");
      ok.addActionListener(new OkListener());
      chooser.getContentPane().add(slider);
      chooser.getContentPane().add(ok);
      chooser.setVisible(true);
      chooser.revalidate();
      chooser.repaint();
      while (valid_ == 0)
        ;
      zone_ = slider.getValue();
    }
  }*/

  private String name_;
  private int zone_;

  public BinarizeFilter()
  {
    name_ = "Binarize";
    zone_ = 15;
  }

  private int[] binarize(int x, int y, BufferedImage img)
  {

    int[] res = new int[4];
    int max = 0;
    int min = 255;
    int moy = 0;
    int tmp = 0;

    for (int i = x - (x < zone_ / 2 ? x : zone_ / 2); i < x
        + (x > img.getWidth() - 1 - zone_ / 2 ? img.getWidth() - 1 - x
            : zone_ / 2); i++)
    {
      for (int j = y - (y < zone_ / 2 ? y : zone_ / 2); j < y
          + (y > img.getHeight() - 1 - zone_ / 2 ? img.getHeight() - 1 - y
              : zone_ / 2); j++)
      {
        try
        {
          tmp = Utils.extractComp(1, img.getRGB(i, j));
        } catch (Exception e)
        {
          System.out.println("x : " + x + " y : " + y);
          System.out.println("i : " + i + " j : " + j);
          e.printStackTrace();
          System.exit(1);
        }

        if (tmp > max)
          max = tmp;
        if (tmp < min)
          min = tmp;
      }
    }

    tmp = Utils.extractComp(1, img.getRGB(x, y));
    moy = (max + min) / 2;

    if (tmp > moy)
      tmp = 255;
    else
      tmp = 0;

    res[0] = Utils.extractComp(0, img.getRGB(x, y));

    for (int i = 1; i < 4; i++)
    {
      res[i] = tmp;
    }

    return res;
  }

  @Override
  public BufferedImage perform(BufferedImage img)
  {
    img = (new GrayscaleFilter()).perform(img);
    BufferedImage result = Utils.copyImage(img);
    int[] res = new int[4];

    for (int i = 0; i < img.getWidth(); i++)
    {
      for (int j = 0; j < img.getHeight(); j++)
      {
        /*for (int c = 0; c < 4; c++)
        {
          res[c] = Utils.extractComp(c, img.getRGB(i, j));
        }*/

        result.setRGB(i, j, Utils.mergeComp(binarize(i, j, img)));
      }
    }

    return result;
  }

  @Override
  public String getName()
  {
    return name_;
  }
}
