package plugin.basic;

import java.awt.image.BufferedImage;
import plugin.IPlugin;
import plugin.Utils;

public class AspireFilter implements IPlugin
{
  private String name_;

  public AspireFilter()
  {
    name_ = "Aspire";
  }

  private int tox(double r, double a)
  {
    return (int) (r * Math.cos(a));
  }

  private int toy(double r, double a)
  {
    return (int) (r * Math.sin(a));
  }

  public BufferedImage perform(BufferedImage img)
  {
    BufferedImage result = Utils.copyImage(img);

    for (int i = 1; i < img.getWidth(); i++)
    {
      for (int j = 1; j < img.getHeight(); j++)
      {
        double r = Math.sqrt(Math.pow(i, 2) + Math.pow(j, 2));
        double a = Math.acos(r / (double) i);

        result.setRGB(i, j,
            img.getRGB(tox(Math.sqrt(r * 400), a), toy(Math.sqrt(r * 400), a)));
      }
    }

    return result;
  }

  @Override
  public String getName()
  {
    return name_;
  }
}
