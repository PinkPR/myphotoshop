package plugin.basic;

import java.awt.image.BufferedImage;

import plugin.IPlugin;

public class RotateRightFilter implements IPlugin
{
  private String name_;

  public RotateRightFilter()
  {
    name_ = "RotateRight";
  }

  @Override
  public BufferedImage perform(BufferedImage img)
  {
    BufferedImage result = new BufferedImage(img.getHeight(), img.getWidth(), BufferedImage.TYPE_INT_ARGB);

    for (int i = 0; i < img.getWidth(); i++)
    {
      for (int j = 0; j < img.getHeight(); j++)
      {
        result.setRGB(img.getHeight() - 1 - j, i, img.getRGB(i, j));
      }
    }

    return result;
  }

  @Override
  public String getName()
  {
    return name_;
  }
}
