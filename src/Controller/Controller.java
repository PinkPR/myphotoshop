package Controller;

import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JFileChooser;

import plugin.IPlugin;
import GUI.GUI;
import History.Memento;
import ImageRW.IO;

public class Controller
{
  private GUI gui_;
  private Memento memento_;

  public Controller(GUI gui)
  {
    gui_ = gui;
    memento_ = gui.getMemento();
  }

  public void applyFilterAndUpdate(IPlugin filter)
  {
    System.out.println("ici");
    memento_.push(filter.perform(memento_.getImage()));
    System.out.println("here");
    gui_.updateImage(memento_.getImage());
  }

  public void control(ActionEvent e)
  {
    System.out.println("je suis la");

    JButton butt = (JButton) e.getSource();

    if (butt.getName().equals("Undo"))
    {
      System.out.println("Undo");
      memento_.undo();
      gui_.updateImage(memento_.getImage());
    } else if (butt.getName().equals("Redo"))
    {
      System.out.println("Redo");
      memento_.redo();
      gui_.updateImage(memento_.getImage());
    } else if (butt.getName().equals("File"))
    {
      JFileChooser fc = new JFileChooser();
      fc.showOpenDialog(null);
      if (fc.getSelectedFile() != null)
      {
        memento_.push(IO.ReadImage(fc.getSelectedFile()));
        gui_.updateImage(memento_.getImage());
      }
    }
  }
}
