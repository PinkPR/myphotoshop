package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.JMenuItem;

import plugin.IPlugin;
import GUI.GUI;

public class FilterMenuController implements ActionListener
{
  public class FilterThread implements Runnable
  {
    private IPlugin filter;

    public FilterThread(IPlugin filterr)
    {
      filter = filterr;
    }

    public void run()
    {
      BufferedImage img = filter.perform(gui_.getCurrentTab().getMemento()
          .getImage());
      gui_.cancel_.setEnabled(false);
      gui_.getCurrentTab().getMemento().push(img);
      gui_.getCurrentTab().updateImage(
          gui_.getCurrentTab().getMemento().getImage());
    }
  }

  private GUI gui_;
  private Thread t_;
  private CancelController cancel_control_;
  private boolean control_;

  public FilterMenuController(GUI gui)
  {
    gui_ = gui;
    cancel_control_ = new CancelController(gui_, this);
    control_ = false;
  }

  public void applyFilterAndUpdate(IPlugin filter)
  {

    System.out.println("Current tab : " + gui_.getCurrentTab().getName());
    System.out.println("stack ptr : "
        + gui_.getCurrentTab().getMemento().getStack().getPtr());
    gui_.getCurrentTab().getMemento()
        .push(filter.perform(gui_.getCurrentTab().getMemento().getImage()));
    gui_.getCurrentTab().updateImage(
        gui_.getCurrentTab().getMemento().getImage());
  }

  @Override
  public void actionPerformed(ActionEvent e)
  {
    if (!control_)
    {
      gui_.cancel_.addActionListener(cancel_control_);
    }

    JMenuItem b = (JMenuItem) e.getSource();
    t_ = new Thread(new FilterThread(gui_.getFilterMenu()
        .getPlugin(b.getText())));
    gui_.cancel_.setEnabled(true);
    gui_.cancel_.revalidate();
    gui_.cancel_.repaint();
    //b.setSelected(false);
    gui_.revalidate();
    gui_.repaint();
    t_.start();
    System.out.println("here");
    //applyFilterAndUpdate(gui_.getFilterMenu().getPlugin(b.getText()));
  }

  public Thread getThread()
  {
    return t_;
  }
}
