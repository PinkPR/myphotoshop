package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import GUI.GUI;

public class CancelController implements ActionListener
{
  private GUI gui_;
  private FilterMenuController filter_menu_;

  public CancelController(GUI gui, FilterMenuController filter_menu)
  {
    gui_ = gui;
    filter_menu_ = filter_menu;
  }

  public void actionPerformed(ActionEvent e)
  {
    JButton butt = (JButton) e.getSource();

    if (butt.getText().equals("Cancel"))
    {
      filter_menu_.getThread().stop();
      gui_.cancel_.setEnabled(false);
    }
  }
}
