package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.jar.JarFile;

import javax.swing.JFileChooser;
import javax.swing.JMenuItem;

import plugin.IPlugin;
import serialize.Serializer;
import GUI.GUI;
import GUI.ImageTab;
import History.Memento;
import ImageRW.IO;

public class ButtonController implements ActionListener
{
  private GUI gui_;
  private Memento memento_;

  public ButtonController(GUI gui)
  {
    gui_ = gui;
    memento_ = gui.getMemento();
  }

  public void applyFilterAndUpdate(IPlugin filter)
  {
    memento_.push(filter.perform(memento_.getImage()));
    gui_.updateImage(memento_.getImage());
  }

  @Override
  public void actionPerformed(ActionEvent e)
  {
    JMenuItem item = (JMenuItem) e.getSource();
    System.out.println(item.getText());

    if (item.getText().equals("Undo"))
    {
      System.out.println("Undo");
      gui_.getCurrentTab().getMemento().undo();
      gui_.getCurrentTab().updateImage(
          gui_.getCurrentTab().getMemento().getImage());
      //memento_.undo();
      //gui_.updateImage(memento_.getImage());
    } else if (item.getText().equals("Redo"))
    {
      System.out.println("Redo");
      gui_.getCurrentTab().getMemento().redo();
      gui_.getCurrentTab().updateImage(
          gui_.getCurrentTab().getMemento().getImage());
      //memento_.redo();
      //gui_.updateImage(memento_.getImage());
    } else if (item.getText().equals("Open image"))
    {
      JFileChooser fc = new JFileChooser();
      fc.showOpenDialog(null);
      if (fc.getSelectedFile() != null)
      {
        BufferedImage img = IO.ReadImage(fc.getSelectedFile());
        ImageTab imgtab = new ImageTab(img, fc.getSelectedFile().getName());
        imgtab.getMemento().push(img);
        System.out.println(imgtab.getMemento().getStack().getPtr());
        //memento_.push(IO.ReadImage(fc.getSelectedFile()));
        //gui_.updateImage(memento_.getImage());
        gui_.addTab(imgtab);
      }
    } else if (item.getText().equals("Save image"))
    {
      JFileChooser fc = new JFileChooser();
      fc.showSaveDialog(null);
      if (fc.getSelectedFile() != null)
      {
        IO.SaveImage(fc.getSelectedFile(), gui_.getCurrentTab().getMemento()
            .getImage());
      }
    } else if (item.getText().equals("Open project"))
    {
      JFileChooser fc = new JFileChooser();
      fc.showOpenDialog(null);
      if (fc.getSelectedFile() != null)
      {
        Memento memento = Serializer.Deserialize(fc.getSelectedFile());
        ImageTab imgtab = new ImageTab(memento.getImage(), fc.getSelectedFile()
            .getName());
        imgtab.setMemento(memento);
        gui_.addTab(imgtab);
        //gui_.getCurrentTab().setMemento(Serializer.Deserialize(fc.getSelectedFile()));
        //gui_.getCurrentTab().updateImage(gui_.getCurrentTab().getMemento().getImage());
        //memento_ = gui_.getMemento();
        //gui_.updateImage(gui_.getMemento().getImage());
      }
    } else if (item.getText().equals("Save project"))
    {
      JFileChooser fc = new JFileChooser();
      fc.showSaveDialog(null);
      if (fc.getSelectedFile() != null)
      {
        Serializer.Serialize(gui_.getCurrentTab().getMemento(),
            fc.getSelectedFile());
      }
    } else if (item.getText().equals("Open JAR file"))
    {
      JFileChooser fc = new JFileChooser();
      fc.showOpenDialog(null);
      if (fc.getSelectedFile() != null)
      {
        try
        {
          gui_.getLoader().loadClasses(new JarFile(fc.getSelectedFile()));
          gui_.getLoader().addFilter(gui_.getFilterMenu(),
              gui_.getMenuController());
        } catch (Exception e1)
        {
          System.err.println("Fail on JAR file opening");
          e1.printStackTrace();
        }
      }
    } else if (item.getText().equals("Close tab"))
    {
      gui_.getTabPane().remove(gui_.getCurrentTab());
    } else if (item.getText().equals("Exit MyPhotoshop"))
    {
      System.exit(0);
    }
  }
}
