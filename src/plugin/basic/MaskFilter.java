package plugin.basic;

import java.awt.image.BufferedImage;

import plugin.IPlugin;
import plugin.Utils;

public class MaskFilter implements IPlugin
{
  private String name_;
  private int[] mask_;

  public MaskFilter()
  {
    name_ = "Mask";
    mask_ = new int[3];

    mask_[0] = 128;
    mask_[1] = 128;
    mask_[2] = 128;
  }

  private int[] maskColors(int[] orig)
  {
    for (int i = 1; i < 4; i++)
    {
      orig[i] = orig[i] & mask_[i - 1];
    }

    return orig;
  }

  @Override
  public BufferedImage perform(BufferedImage img)
  {
    BufferedImage result = Utils.copyImage(img);
    int[] res = new int[4];

    for (int i = 0; i < img.getWidth(); i++)
    {
      for (int j = 0; j < img.getHeight(); j++)
      {
        for (int c = 0; c < 4; c++)
        {
          res[c] = Utils.extractComp(c, img.getRGB(i, j));
        }

        res = maskColors(res);
        result.setRGB(i, j, Utils.mergeComp(res));
      }
    }

    return result;
  }

  @Override
  public String getName()
  {
    return name_;
  }
}
