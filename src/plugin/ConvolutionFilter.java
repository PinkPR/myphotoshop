package plugin;

import java.awt.image.BufferedImage;

public class ConvolutionFilter implements IPlugin
{
  int[][] filter_;
  int dim_;
  boolean alpha_;
  String name_;
  int div_;
  int bias_;

  public ConvolutionFilter(int[][] filter, int dim, boolean alpha, String name,
      int div, int bias)
  {
    filter_ = filter;
    dim_ = dim;
    alpha_ = alpha;
    name_ = name;
    div_ = div;
    bias_ = bias;

    /*System.out.println("Filter :");
		for (int i = 0; i < dim_; i++)
		{
			for (int j = 0; j < dim_; j++)
			{
				System.out.print(filter_[j][i] + " ");
			}

			System.out.println();
		}*/
  }

  private int getResult(int x, int y, BufferedImage img)
  {
    int[] res = { 0, 0, 0, 0 };

    for (int i = 0; i < dim_; i++)
    {
      for (int j = 0; j < dim_; j++)
      {
        for (int c = 1; c < 4; c++)
        {
          res[c] += filter_[i][j]
              * Utils.extractComp(c,
                  img.getRGB(x - (i - dim_ / 2), y - (j - dim_ / 2)));
        }
      }
    }

    if (!alpha_)
    {
      res[0] = Utils.extractComp(0, img.getRGB(x, y));
    }

    for (int i = 1; i < 4; i++)
    {
      res[i] /= div_;
      res[i] += bias_;
    }

    return Utils.mergeComp(res);
  }

  @Override
  public BufferedImage perform(BufferedImage img)
  {
    BufferedImage new_img = Utils.copyImage(img);

    for (int i = dim_ / 2; i < img.getWidth() - dim_ / 2; i++)
    {
      for (int j = dim_ / 2; j < img.getHeight() - dim_ / 2; j++)
      {
        new_img.setRGB(i, j, getResult(i, j, img));
      }
    }

    return new_img;
  }

  @Override
  public String getName()
  {
    return name_;
  }
}
