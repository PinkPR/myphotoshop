package plugin;

import java.awt.image.BufferedImage;

public class Utils
{
  private static int w_;
  private static int h_;

  public static int extractComp(int comp, int argb)
  {
    int ret = (argb >> (8 * (3 - comp))) & 0x000000FF;

    return ret;
  }

  public static int mergeComp(int[] comps)
  {
    int intres = (comps[0] << (8 * 3)) + (comps[1] << (8 * 2))
        + (comps[2] << (8 * 1)) + comps[3];

    return intres;
  }

  public static BufferedImage copyImage(BufferedImage img)
  {
    return new BufferedImage(img.getWidth(), img.getHeight(),
        BufferedImage.TYPE_INT_ARGB);
  }

  public static BufferedImage resizeImage(BufferedImage img, int max_w,
      int max_h)
  {
    float w_div = (float) img.getWidth() / (float) max_w;
    float h_div = (float) img.getHeight() / (float) max_h;

    float div = 0.0f;
    int w = 0;
    int h = 0;

    if (w_div > h_div)
    {
      div = w_div;
      w = max_w;
      h = (int) ((float) img.getHeight() / div);
    } else
    {
      div = h_div;
      h = max_h;
      w = (int) ((float) img.getWidth() / div);
    }

    BufferedImage new_img = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
    w_ = w;
    h_ = h;

    for (int i = 0; i < w; i++)
    {
      for (int j = 0; j < h; j++)
      {
        new_img.setRGB(i, j,
            img.getRGB((int) ((float) i * div), (int) ((float) j * div)));
      }
    }

    return new_img;
  }

  public int getW()
  {
    return w_;
  }

  public int getH()
  {
    return h_;
  }
}
