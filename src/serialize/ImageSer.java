package serialize;

import java.awt.image.BufferedImage;
import java.io.Serializable;

public class ImageSer implements Serializable
{
  public int w_;
  public int h_;
  public int[] data_;

  public ImageSer(BufferedImage img)
  {
    w_ = img.getWidth();
    h_ = img.getHeight();
    data_ = img.getRGB(0, 0, w_, h_, data_, 0, w_);
  }

  public BufferedImage getBufferedImage()
  {
    BufferedImage img = new BufferedImage(w_, h_, BufferedImage.TYPE_INT_ARGB);
    img.setRGB(0, 0, w_, h_, data_, 0, w_);

    return img;
  }
}
