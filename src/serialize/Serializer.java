package serialize;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import History.Memento;

public class Serializer
{
  public static void Serialize(Memento memento, File file)
  {
    try
    {
      ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
      oos.writeObject(new HistoryStackSer(memento.getStack()));
      oos.close();
    } catch (Exception e)
    {
      System.err.println("Error on saving project");
      e.printStackTrace();
    }
  }

  public static Memento Deserialize(File file)
  {
    try
    {
      ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
      HistoryStackSer stack = (HistoryStackSer) ois.readObject();
      Memento memento = new Memento();
      memento.setStack(stack.getHistoryStack());
      ois.close();

      return memento;
    } catch (Exception e)
    {
      System.err.println("Error on opening project");
      e.printStackTrace();
    }

    return null;
  }
}
