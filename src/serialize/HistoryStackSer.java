package serialize;

import java.io.Serializable;
import java.util.Stack;

import History.HistoryStack;

public class HistoryStackSer implements Serializable
{
  private Stack<ImageSer> stack_;
  private int ptr_;

  public HistoryStackSer(HistoryStack stack)
  {
    stack_ = new Stack<ImageSer>();
    ptr_ = stack.getPtr();

    for (int i = 0; i < stack.getStack().size(); i++)
    {
      stack_.push(new ImageSer(stack.getStack().get(i)));
    }
  }

  public HistoryStack getHistoryStack()
  {
    HistoryStack stack = new HistoryStack();

    for (int i = 0; i < stack_.size(); i++)
    {
      stack.pushHead(stack_.get(i).getBufferedImage());
    }

    return stack;
  }
}
