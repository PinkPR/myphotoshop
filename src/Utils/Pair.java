package Utils;

public class Pair<A, B>
{
  private A a_;
  private B b_;

  public Pair(A a, B b)
  {
    a_ = a;
    b_ = b;
  }

  public void setA(A a)
  {
    a_ = a;
  }

  public void setB(B b)
  {
    b_ = b;
  }

  public A getA()
  {
    return a_;
  }

  public B getB()
  {
    return b_;
  }
}
