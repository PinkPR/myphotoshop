package jarLoader;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import plugin.IPlugin;
import Controller.FilterMenuController;
import GUI.FilterMenu;

public class JarLoader extends ClassLoader
{
  private ArrayList<IPlugin> filters_;

  public JarLoader()
  {
    filters_ = new ArrayList<IPlugin>();
  }

  public void addFilter(FilterMenu menu, FilterMenuController controller)
  {
    while (filters_.size() != 0)
    {
      menu.addItem(filters_.get(filters_.size() - 1), controller);
      filters_.remove(filters_.size() - 1);
    }
  }

  public void copy(InputStream is, OutputStream os)
  {
    int c = 0;

    try
    {
      while ((c = is.read()) != -1)
      {
        os.write(c);
      }
    }
    catch (Exception e)
    {
      System.err.println("Fail on reading class");
    }
  }

  public void reverse(byte[] data)
  {
    for (int i = 0; i < data.length / 2; i++)
    {
      byte tmp = data[i];
      data[i] = data[data.length - 1 - i];
      data[data.length - 1 - i] = tmp;
    }
  }

  public void loadClasses(JarFile file)
  {
    Enumeration entries = file.entries();

    while (entries.hasMoreElements())
    {
      JarEntry entry = (JarEntry) entries.nextElement();

      int index = entry.getName().lastIndexOf('.');
      int begin = entry.getName().lastIndexOf('/');

      if (index != -1)
      {
        index++;
        String end = entry.getName().substring(index);
        String pkg = "plugin.";
        String name = "";

        if (end.equals("class"))
        {
          if (entry.getName().contains("plugin/basic"))
            pkg += "basic.";
          else
            pkg += "bonus.";

          name = entry.getName().substring(begin + 1, index - 1);
          name = pkg + name;
          InputStream is = null;
          try
          {
            is = file.getInputStream(entry);
          } catch (Exception e)
          {
            System.out.println("Class loading failed");
            e.printStackTrace();
          }

          ByteArrayOutputStream os = new ByteArrayOutputStream();
          copy(is, os);
          byte[] data = os.toByteArray();
          //reverse(data);

          Class c = defineClass(name, data, 0, data.length);
          try
          {
            IPlugin plugin = (IPlugin) c.newInstance();
            System.out.println("Class found : " + plugin.getName());
            filters_.add(plugin);
          } catch (Exception e)
          {
            System.out.println("Fail on defining class");
            e.printStackTrace();
          }
        }
      }
    }
  }
}
