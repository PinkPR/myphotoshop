package History;

import java.awt.image.BufferedImage;
import java.io.Serializable;

public class Memento implements Serializable
{
  private HistoryStack stack_;

  public Memento()
  {
    stack_ = new HistoryStack();
  }

  public void push(BufferedImage img)
  {
    stack_.pushHead(img);
  }

  public BufferedImage getImage()
  {
    return stack_.getHeadPtr();
  }

  public void undo()
  {
    stack_.decPtr();
  }

  public void redo()
  {
    stack_.incPtr();
  }

  public HistoryStack getStack()
  {
    return stack_;
  }

  public void setStack(HistoryStack stack)
  {
    stack_ = stack;
  }
}
