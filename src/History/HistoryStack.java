package History;

import java.awt.image.BufferedImage;
import java.util.Stack;

public class HistoryStack
{
  private Stack<BufferedImage> stack_;
  private int ptr_;

  public HistoryStack()
  {
    stack_ = new Stack<BufferedImage>();
    ptr_ = -1;
  }

  public BufferedImage getHead()
  {
    return stack_.get(stack_.size() - 1);
  }

  public BufferedImage getHeadPtr()
  {
    return stack_.get(ptr_);
  }

  public BufferedImage popHead()
  {
    return stack_.pop();
  }

  public void pushHead(BufferedImage elt)
  {
    while (ptr_ >= 0 && ptr_ != stack_.size() - 1)
      stack_.removeElementAt(ptr_ + 1);

    stack_.push(elt);
    ptr_++;
  }

  public void incPtr()
  {
    if (ptr_ < stack_.size() - 1)
      ptr_++;
  }

  public void decPtr()
  {
    if (ptr_ > 0)
      ptr_--;
  }

  public int getPtr()
  {
    return ptr_;
  }

  public Stack<BufferedImage> getStack()
  {
    return stack_;
  }
}
