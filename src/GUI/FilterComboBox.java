package GUI;

import javax.swing.JComboBox;

import plugin.IPlugin;

public class FilterComboBox extends JComboBox
{
  private IPlugin[] filters_;

  @SuppressWarnings("unchecked")
  public FilterComboBox(IPlugin... filters)
  {
    super(getNames(filters));

    filters_ = filters;
  }

  private static String[] getNames(IPlugin[] filters)
  {
    String[] names_ = new String[filters.length];

    for (int i = 0; i < filters.length; i++)
    {
      names_[i] = filters[i].getName();
    }

    return names_;
  }

  public IPlugin getPlugin(String name)
  {
    for (int i = 0; i < filters_.length; i++)
    {
      if (filters_[i].getName().equals(name))
        return filters_[i];
    }

    return null;
  }
}
