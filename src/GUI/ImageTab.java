package GUI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import History.Memento;

public class ImageTab extends JPanel
{
  private String name_;
  private ImagePanel imgpan_;
  private Memento memento_;

  public ImageTab(BufferedImage img, String name)
  {
    super();
    imgpan_ = new ImagePanel(img, this);
    name_ = name;
    memento_ = new Memento();
  }

  @Override
  public void paintComponent(Graphics g)
  {
    imgpan_.paintComponent(g);
  }

  public void updateImage(BufferedImage img)
  {
    if (imgpan_ == null)
    {
      System.out.println("HERE");
      imgpan_ = new ImagePanel(img, this);
      imgpan_.setLayout(new FlowLayout());
    } else
    {
      ImagePanel new_imgpan = new ImagePanel(img, this);
      this.remove(imgpan_);
      imgpan_ = new_imgpan;
    }

    this.add(imgpan_, BorderLayout.CENTER);
    this.revalidate();
    this.repaint();
  }

  @Override
  public String getName()
  {
    return name_;
  }

  public Memento getMemento()
  {
    return memento_;
  }

  public ImagePanel getPanel()
  {
    return imgpan_;
  }

  public void setMemento(Memento memento)
  {
    memento_ = memento;
  }
}
