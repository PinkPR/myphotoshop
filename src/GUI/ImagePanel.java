package GUI;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import plugin.IPlugin;
import plugin.Utils;

public class ImagePanel extends JPanel
{
  private BufferedImage img_;
  private BufferedImage adjusted_img_;
  private GUI gui_;
  private JPanel jpan_;

  public ImagePanel(BufferedImage img, GUI gui)
  {
    img_ = img;
    gui_ = gui;
    jpan_ = null;
    adjusted_img_ = Utils.resizeImage(img_, gui_.getWidth(),
        gui.getHeight() - 25);
  }

  public ImagePanel(BufferedImage img, JPanel jpan)
  {
    img_ = img;
    gui_ = null;
    jpan_ = jpan;
  }

  @Override
  public void paintComponent(Graphics g)
  {
    adjusted_img_ = Utils.resizeImage(img_, jpan_.getWidth(),
        jpan_.getHeight() - 25);
    super.paintComponent(g);
    Graphics2D g2d = (Graphics2D) g;
    if (gui_ != null)
    {
      g2d.drawImage(adjusted_img_, null,
          (gui_.getWidth() - adjusted_img_.getWidth()) / 2,
          (gui_.getHeight() - 25 - adjusted_img_.getHeight()) / 2);
    } else
    {
      g2d.drawImage(adjusted_img_, null,
          (jpan_.getWidth() - adjusted_img_.getWidth()) / 2,
          (jpan_.getHeight() - /*25 -*/adjusted_img_.getHeight()) / 2);
    }
  }

  public void performFilter(IPlugin filter)
  {
    img_ = filter.perform(img_);
    updateUI();
  }

  public BufferedImage getImage()
  {
    return img_;
  }
}
