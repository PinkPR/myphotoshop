package GUI;

import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import plugin.IPlugin;

public class FilterMenu extends JMenu
{
  private ArrayList<IPlugin> filters_;

  public FilterMenu(String text)
  {
    super(text);
    filters_ = new ArrayList<IPlugin>();
  }

  public void addItem(IPlugin filter, ActionListener al)
  {
    filters_.add(filter);
    JMenuItem new_filter = new JMenuItem(filter.getName());
    new_filter.addActionListener(al);
    this.add(new_filter);
    this.revalidate();
    this.repaint();
  }

  public IPlugin getPlugin(String name)
  {
    for (int i = 0; i < filters_.size(); i++)
    {
      if (filters_.get(i).getName().equals(name))
        return filters_.get(i);
    }

    return null;
  }
}
