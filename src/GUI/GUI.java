package GUI;

import jarLoader.JarLoader;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.jar.JarFile;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;

import Controller.ButtonController;
import Controller.FilterMenuController;
import History.Memento;

public class GUI extends JFrame implements ActionListener
{
  private ImagePanel imgpan;
  private Memento memento_;
  private ActionListener button_control_;
  private FilterMenuController filter_control_;
  private FilterMenu filter_menu_;
  private JTabbedPane tabpan_;
  private JarLoader loader_;
  public JButton cancel_;

  public GUI(BufferedImage img)
  {
    memento_ = new Memento();
    button_control_ = new ButtonController(this);
    filter_control_ = new FilterMenuController(this);
    filter_menu_ = new FilterMenu("Filters");
    imgpan = null;
    tabpan_ = new JTabbedPane();
    loader_ = new JarLoader();
    this.getContentPane().add(tabpan_, BorderLayout.CENTER);

    this.setSize(800, 600);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    this.setLayout(new BorderLayout());

    JMenuBar menubar = new JMenuBar();
    JMenu file = new JMenu("File");
    JMenu edit = new JMenu("Edit");

    menubar.add(file);
    menubar.add(edit);
    menubar.add(filter_menu_);
    this.setJMenuBar(menubar);

    cancel_ = new JButton("Cancel");
    cancel_.setEnabled(false);
    this.getContentPane().add(cancel_, BorderLayout.SOUTH);

    // File Menu Items
    JMenuItem file_open_image = new JMenuItem("Open image");
    JMenuItem file_save_image = new JMenuItem("Save image");
    JMenuItem file_open_project = new JMenuItem("Open project");
    JMenuItem file_save_project = new JMenuItem("Save project");
    JMenuItem file_open_jar = new JMenuItem("Open JAR file");
    JMenuItem file_close_tab = new JMenuItem("Close tab");
    JMenuItem file_exit = new JMenuItem("Exit MyPhotoshop");

    file_open_image.addActionListener(button_control_);
    file_save_image.addActionListener(button_control_);
    file_open_project.addActionListener(button_control_);
    file_save_project.addActionListener(button_control_);
    file_open_jar.addActionListener(button_control_);
    file_close_tab.addActionListener(button_control_);
    file_exit.addActionListener(button_control_);

    file.add(file_open_image);
    file.add(file_open_project);
    file.addSeparator();
    file.add(file_save_image);
    file.add(file_save_project);
    file.addSeparator();
    file.add(file_open_jar);
    file.addSeparator();
    file.add(file_close_tab);
    file.addSeparator();
    file.add(file_exit);

    file_open_project.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,
        ActionEvent.CTRL_MASK));
    file_save_project.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
        ActionEvent.CTRL_MASK));
    file_exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,
        ActionEvent.CTRL_MASK));

    // Edit Menu Items
    JMenuItem edit_undo = new JMenuItem("Undo");
    JMenuItem edit_redo = new JMenuItem("Redo");

    edit_undo.addActionListener(button_control_);
    edit_redo.addActionListener(button_control_);

    edit.add(edit_undo);
    edit.add(edit_redo);

    edit_undo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z,
        ActionEvent.CTRL_MASK));
    edit_redo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y,
        ActionEvent.CTRL_MASK));

    this.setVisible(true);
  }

  public void updateImage(BufferedImage img)
  {
    if (imgpan == null)
    {
      System.out.println("HERE");
      imgpan = new ImagePanel(img, this);
      imgpan.setLayout(new FlowLayout());
    } else
    {
      ImagePanel new_imgpan = new ImagePanel(img, this);
      this.remove(imgpan);
      imgpan = new_imgpan;
    }

    this.getContentPane().add(imgpan, BorderLayout.CENTER);
    this.revalidate();
    this.repaint();
  }

  public void componentResized(ComponentEvent e)
  {
    System.out.println("resized");
    ImagePanel new_imgpan = new ImagePanel(imgpan.getImage(), this);
    this.remove(imgpan);
    imgpan = new_imgpan;
    this.getContentPane().add(imgpan, BorderLayout.CENTER);
    this.revalidate();
    this.repaint();
  }

  public void addTab(ImageTab imgtab)
  {
    tabpan_.addTab(imgtab.getName(), imgtab);
    this.getContentPane().add(tabpan_);
    tabpan_.revalidate();
    this.revalidate();
    imgtab.getPanel().revalidate();
    imgtab.getPanel().repaint();
    System.out.println("imgtab pos : " + imgtab.getPanel().getX() + " "
        + imgtab.getPanel().getY());
  }

  public Memento getMemento()
  {
    return memento_;
  }

  public FilterMenu getFilterMenu()
  {
    return filter_menu_;
  }

  public FilterMenuController getMenuController()
  {
    return filter_control_;
  }

  public JarLoader getLoader()
  {
    return loader_;
  }

  public JTabbedPane getTabPane()
  {
    return tabpan_;
  }

  public ImageTab getCurrentTab()
  {
    return (ImageTab) tabpan_.getSelectedComponent();
  }

  public void setMemento(Memento memento)
  {
    memento_ = memento;
  }

  @Override
  public void actionPerformed(ActionEvent e)
  {
  }
}
