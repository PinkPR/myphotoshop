package ImageRW;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

public class IO
{
  public static BufferedImage ReadImage(String file)
  {
    try
    {
      BufferedImage bfimg = ImageIO.read(new File(file));
      return bfimg;
    } catch (Exception e)
    {
      System.out.println("Image loading failed.");
      e.printStackTrace();
    }

    return null;
  }

  public static BufferedImage ReadImage(File file)
  {
    try
    {
      BufferedImage bfimg = ImageIO.read(file);
      return bfimg;
    } catch (Exception e)
    {
      System.out.println("Image loading failed.");
      e.printStackTrace();
    }

    return null;
  }

  public static void SaveImage(File file, BufferedImage img)
  {
    try
    {
      String format = file.getName().substring(file.getName().indexOf('.') + 1);
      System.out.println(format);
      ImageIO.write(img, format, file);
      System.out.println("image saved.");
    } catch (Exception e)
    {
      System.out.println("Image saving failed.");
      e.printStackTrace();
    }
  }
}
